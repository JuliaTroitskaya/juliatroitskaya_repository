import com.codeborne.selenide.Condition;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.Assert.assertEquals;

public class MainPageTest {

    @Test
    public void checkEmailFieldOnAccountCreationForm () throws InterruptedException {
        open ("https://rabota.ua/ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
        $ (byXpath ("//*[@id=\"newheader\"]/div/div[2]/div/div[2]/ul/li[3]/a/label")).click ();
        $ (byId ("ctl00_Sidebar_login_txbLogin")).should (Condition.exist).shouldHave (type ("email")).shouldBe (empty);
    }

    @Test
    public void checkErrorMessageOnAccountCreationForm () throws InterruptedException {
        open ("https://rabota.ua/ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
        $ (byXpath ("//*[@id=\"newheader\"]/div/div[2]/div/div[2]/ul/li[3]/a/label")).click ();
        $ (byId ("ctl00_Sidebar_login_lnkLogin")).pressEnter ();
        $ (byXpath ("//*[@id=\"divSidebarName\"]/span")).shouldHave (exactText ("Поле, обов`язкове для заповнення"));
    }

    @Test
    public void selectRubric () throws InterruptedException {
        open ("https://rabota.ua/ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
        $ (byName ("ctl00$content$VerticalContainer$ddlRubrics")).selectOption ("IT");
        $ (byText ("Game Development")).shouldNotBe (Condition.checked);
    }

    @Test
    public void checkHeaderInSearchResults () throws InterruptedException {
        open ("https://rabota.ua/ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
        $ (byClassName ("ui-autocomplete-input")).setValue ("test engineer").pressEnter ();
        $ (byXpath ("//*[@id=\"aspnetForm\"]/div[4]/div/section/div[1]/h1")).shouldHave (text ("engineer"));
    }

    @Test
    public void checkURLonLocaleSwitching () throws InterruptedException {
        open ("https://rabota.ua/ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
        $ (byXpath ("//*[@id=\"newheader\"]/div/div[2]/div/div[1]/span[3]")).click ();
        assertEquals (url (), "https://rabota.ua/%D1%85%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2");
    }

}
